let mongo = require('mongodb')
let Chance = require('chance')
let chance = new Chance()

let url = 'mongodb://localhost:27017/water-company'
let db
/////CONFIGURATION/////
let numberOfMeters = 10
let interval = 3600000

let checkIfMetersExist = () => {
  return db.collection('meters')
    .count()
    .then((count) => {
      if (count < numberOfMeters) {
        return createMeters(numberOfMeters - count)
      }
    })
    .then(() => {
      startInterval()
    })
}

let findMeters = () => {
  db.collection('meters')
    .find({}, { id: 1, lastCumulativeRead: 1 })
    .limit(numberOfMeters)
    .toArray()
    .then((meters) =>{
      generateReadings(meters).catch((error) => {
        console.log(error)
      })
    })
}

let createMeters = (number) => {
  console.log('creating ' + number + ' meters')
  let meters = []

  db.collection('meters').createIndex( { 'endpointId': 1 }, { 'unique': true } )

  for (let i = 0; i < number; i++) {
    meters.push(
      { 
        name: chance.name(), 
        lastCumulativeRead: chance.floating({ min: 0, max: 100, fixed: 4}),
        endpointId: chance.hash({ length: 10 })
      }
    )
  }

  db.collection('meters').insertMany(meters, { ordered: false })
    .catch((error) => {
      console.log('Conflict found with endpoint ID-s. ' + error.writeErrors.length + ' meters skipped!')
    })
}

let generateReadings = (meters) => {
  let promiseList = []

  meters.forEach((meter) => {
    let newReading = chance.floating({ min: 0, max: 0.5, fixed: 4})

    let event = {
      meterId: mongo.ObjectId(meter._id),
      date: new Date(),
      reading: newReading
    }

    let promise = db.collection('readings')
      .insertOne(event)
      .then(() => {
        console.log('new reading event inserted for meter ' + meter._id)
        return db.collection('meters').updateOne({ _id: mongo.ObjectId(meter._id) }, { $set: { lastCumulativeRead: newReading + meter.lastCumulativeRead } })
      })

    promiseList.push(promise)
  })

  return Promise.all(promiseList)
}

let startInterval = () => {
  setInterval(() => {
    findMeters()
  }, interval)
}

mongo.connect(url)
  .then((database) => {
    db = database
    return checkIfMetersExist()
  })
  .catch((error) => {
    console.log(error)
  })

console.log('Generator started')